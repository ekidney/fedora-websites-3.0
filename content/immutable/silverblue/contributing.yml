path: contributing
title: Contributing
description: Join a community of users and contributors focused on Fedora Silverblue.
sections:
  - sectionTitle: Description
    sectionDescription:
      Fedora Silverblue is a community-driven project seeking contributions from folks with any skillset.
      From documentation writing, to beta testing, to helping others by answering questions,
      there are plenty of ways to get involved and help shape the future of Silverblue!
    content: []
  - sectionTitle: Communication channels
    content:
      - title: Forum
        description: The **Fedora Silverblue** tag on Fedora's Discourse-based
          discussion forum is a great place to connect with other Fedora
          Silverblue users and contributors.
        image: public/assets/images/fedora-discussion-plus-icon.png
        link:
          text: Visit Now
          url: https://discussion.fedoraproject.org/tag/silverblue
      - title: Chat
        description:
          "You can chat with the Fedora Silverblue community via either
          Matrix or IRC: **#silverblue:fedoraproject.org** on Matrix or
          #silverblue on irc.libera.chat."
        image: public/assets/images/fedora-chat-plus-element.png
        link:
          text: Chat Now
          url: https://chat.fedoraproject.org/#/room/#silverblue:fedoraproject.org
  - sectionTitle: Ways to get involved
    content:
      - title: Developers
        description:
          Fedora Silverblue is powered by some amazing open-source projects, including
          [ostree](https://github.com/ostreedev/ostree), [rpm-ostree](https://github.com/coreos/rpm-ostree), and [flatpak](https://github.com/flatpak/flatpak). If you are
          a developer, consider helping out with one of these projects.
        image: public/assets/images/computer.svg
      - title: Writers
        description:
          Fedora Silverblue utilizes interesting, cutting-edge technology, and we need to tell more people about it!
          If you're a keen writer, contributing to the
          [Fedora Silverblue documentation](https://github.com/fedora-silverblue/silverblue-docs) will help others learn how their
          system works, and all the cool things they can do with it.
        image: public/assets/images/writing-notepad.svg
      - title: Testers
        description:
          Found something that isn't quite right? Want to propose a change to the way Silverblue works? We want to hear from you!
          Head over to the [Fedora Silverblue issue tracker](https://github.com/fedora-silverblue/issue-tracker) and let us know.
        image: public/assets/images/bug.svg

  - sectionTitle: Fedora Publications
    content:
      - title: Fedora Community Blog
        description:
          The Community Blog provides a single source for members of the
          community to share important news, updates, and information about
          Fedora with others in the Project community.
        image: public/assets/images/community-blog.png
        link:
          text: Visit Now
          url: https://communityblog.fedoraproject.org/
      - title: Fedora Magazine
        description:
          Fedora Magazine is a website that hosts promotional articles and
          short guides contributed from the community about free/libre and
          open-source software that runs on or works with the Fedora Linux
          operating system.
        image: public/assets/images/fedora-magazine.png
        link:
          text: Visit now
          url: https://fedoramagazine.org/
      - title: Become a Fedora contributor
        description:
          "Both the Fedora Community Blog and Fedora Magazine are always
          [looking for article proposals, writers, and
          editors](https://docs.fedoraproject.org/en-US/fedora-magazine/contrib\
          uting/). "
